package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the fab_cliente database table.
 * 
 */
@Entity
@Table(name="fab_cliente")
@NamedQuery(name="FabCliente.findAll", query="SELECT f FROM FabCliente f")
public class FabCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_fab_cliente", unique=true, nullable=false)
	private Integer idFabCliente;

	@Column(nullable=false)
	private Boolean activo;

	@Column(nullable=false, length=50)
	private String apellidos;

	@Column(nullable=false, length=10)
	private String cedula;

	@Column(nullable=false, length=50)
	private String clave;

	@Column(nullable=false, length=50)
	private String correo;

	@Column(nullable=false, length=50)
	private String nombres;

	public FabCliente() {
	}

	public Integer getIdFabCliente() {
		return this.idFabCliente;
	}

	public void setIdFabCliente(Integer idFabCliente) {
		this.idFabCliente = idFabCliente;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

}